<?php
/*
Plugin Name: HipsterLab
Plugin URI: http://hipsterlab.com/
Description: HipsterLab Business Core
Version: 1.0.0
Author: HipsterLab
Author URI: http://hipsterlab.com/
License: GPLv2 or later
Text Domain: industrial
*/
// Register Custom Post Type
function industrial_post_type_static_block() {

	// Static Post Type
	$labels = array(
		'name'                  => _x( 'Static Blocks', 'Post Type General Name', 'industrial' ),
		'singular_name'         => _x( 'Static Block', 'Post Type Singular Name', 'industrial' ),
		'menu_name'             => __( 'Static Blocks', 'industrial' ),
		'name_admin_bar'        => __( 'Static Block', 'industrial' ),
		'archives'              => __( 'Static Block Archives', 'industrial' ),
		'parent_item_colon'     => __( 'Parent Item:', 'industrial' ),
		'all_items'             => __( 'All Items', 'industrial' ),
		'add_new_item'          => __( 'Add New Item', 'industrial' ),
		'add_new'               => __( 'Add New', 'industrial' ),
		'new_item'              => __( 'New Item', 'industrial' ),
		'edit_item'             => __( 'Edit Item', 'industrial' ),
		'update_item'           => __( 'Update Item', 'industrial' ),
		'view_item'             => __( 'View Item', 'industrial' ),
		'search_items'          => __( 'Search Item', 'industrial' ),
		'not_found'             => __( 'Not found', 'industrial' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'industrial' ),
		'featured_image'        => __( 'Featured Image', 'industrial' ),
		'set_featured_image'    => __( 'Set featured image', 'industrial' ),
		'remove_featured_image' => __( 'Remove featured image', 'industrial' ),
		'use_featured_image'    => __( 'Use as featured image', 'industrial' ),
		'insert_into_item'      => __( 'Insert into item', 'industrial' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'industrial' ),
		'items_list'            => __( 'Items list', 'industrial' ),
		'items_list_navigation' => __( 'Items list navigation', 'industrial' ),
		'filter_items_list'     => __( 'Filter items list', 'industrial' ),
	);
	$args = array(
		'label'                 => __( 'Static Block', 'industrial' ),
		'description'           => __( 'Static Block', 'industrial' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-editor-kitchensink',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'static_block', $args );
	// Services Post Type
	$labels = array(
		'name'                  => _x( 'Services', 'Post Type General Name', 'industrial' ),
		'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'industrial' ),
		'menu_name'             => __( 'Services', 'industrial' ),
		'name_admin_bar'        => __( 'Service', 'industrial' ),
		'archives'              => __( 'Services Archives', 'industrial' ),
		'parent_item_colon'     => __( 'Parent Item:', 'industrial' ),
		'all_items'             => __( 'All Items', 'industrial' ),
		'add_new_item'          => __( 'Add New Item', 'industrial' ),
		'add_new'               => __( 'Add New', 'industrial' ),
		'new_item'              => __( 'New Item', 'industrial' ),
		'edit_item'             => __( 'Edit Item', 'industrial' ),
		'update_item'           => __( 'Update Item', 'industrial' ),
		'view_item'             => __( 'View Item', 'industrial' ),
		'search_items'          => __( 'Search Item', 'industrial' ),
		'not_found'             => __( 'Not found', 'industrial' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'industrial' ),
		'featured_image'        => __( 'Featured Image', 'industrial' ),
		'set_featured_image'    => __( 'Set featured image', 'industrial' ),
		'remove_featured_image' => __( 'Remove featured image', 'industrial' ),
		'use_featured_image'    => __( 'Use as featured image', 'industrial' ),
		'insert_into_item'      => __( 'Insert into item', 'industrial' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'industrial' ),
		'items_list'            => __( 'Items list', 'industrial' ),
		'items_list_navigation' => __( 'Items list navigation', 'industrial' ),
		'filter_items_list'     => __( 'Filter items list', 'industrial' ),
	);
	$args = array(
		'label'                 => __( 'Service', 'industrial' ),
		'description'           => __( 'Service', 'industrial' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail','excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-analytics',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
	);
	register_post_type( 'services', $args );
	// Testimonials
	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'industrial' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'industrial' ),
		'menu_name'             => __( 'Testimonials', 'industrial' ),
		'name_admin_bar'        => __( 'Testimonial', 'industrial' ),
		'archives'              => __( 'Testimonials Archives', 'industrial' ),
		'parent_item_colon'     => __( 'Parent Item:', 'industrial' ),
		'all_items'             => __( 'All Items', 'industrial' ),
		'add_new_item'          => __( 'Add New Item', 'industrial' ),
		'add_new'               => __( 'Add New', 'industrial' ),
		'new_item'              => __( 'New Item', 'industrial' ),
		'edit_item'             => __( 'Edit Item', 'industrial' ),
		'update_item'           => __( 'Update Item', 'industrial' ),
		'view_item'             => __( 'View Item', 'industrial' ),
		'search_items'          => __( 'Search Item', 'industrial' ),
		'not_found'             => __( 'Not found', 'industrial' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'industrial' ),
		'featured_image'        => __( 'Featured Image', 'industrial' ),
		'set_featured_image'    => __( 'Set featured image', 'industrial' ),
		'remove_featured_image' => __( 'Remove featured image', 'industrial' ),
		'use_featured_image'    => __( 'Use as featured image', 'industrial' ),
		'insert_into_item'      => __( 'Insert into item', 'industrial' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'industrial' ),
		'items_list'            => __( 'Items list', 'industrial' ),
		'items_list_navigation' => __( 'Items list navigation', 'industrial' ),
		'filter_items_list'     => __( 'Filter items list', 'industrial' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'industrial' ),
		'description'           => __( 'Testimonial', 'industrial' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'publicly_queryable'    => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-testimonial',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
	);
	register_post_type( 'testimonials', $args );
	// Staff
	$labels = array(
		'name'                  => _x( 'Staff', 'Post Type General Name', 'industrial' ),
		'singular_name'         => _x( 'Staff', 'Post Type Singular Name', 'industrial' ),
		'menu_name'             => __( 'Staff', 'industrial' ),
		'name_admin_bar'        => __( 'Staff', 'industrial' ),
		'archives'              => __( 'Staff Archives', 'industrial' ),
		'parent_item_colon'     => __( 'Parent Item:', 'industrial' ),
		'all_items'             => __( 'All Items', 'industrial' ),
		'add_new_item'          => __( 'Add New Item', 'industrial' ),
		'add_new'               => __( 'Add New', 'industrial' ),
		'new_item'              => __( 'New Item', 'industrial' ),
		'edit_item'             => __( 'Edit Item', 'industrial' ),
		'update_item'           => __( 'Update Item', 'industrial' ),
		'view_item'             => __( 'View Item', 'industrial' ),
		'search_items'          => __( 'Search Item', 'industrial' ),
		'not_found'             => __( 'Not found', 'industrial' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'industrial' ),
		'featured_image'        => __( 'Featured Image', 'industrial' ),
		'set_featured_image'    => __( 'Set featured image', 'industrial' ),
		'remove_featured_image' => __( 'Remove featured image', 'industrial' ),
		'use_featured_image'    => __( 'Use as featured image', 'industrial' ),
		'insert_into_item'      => __( 'Insert into item', 'industrial' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'industrial' ),
		'items_list'            => __( 'Items list', 'industrial' ),
		'items_list_navigation' => __( 'Items list navigation', 'industrial' ),
		'filter_items_list'     => __( 'Filter items list', 'industrial' ),
	);
	$args = array(
		'label'                 => __( 'Staff', 'industrial' ),
		'description'           => __( 'Staff', 'industrial' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'excerpt', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'publicly_queryable'    => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
	);
	register_post_type( 'staff', $args );
}
add_action( 'init', 'industrial_post_type_static_block', 0 );
// Widget
require_once plugin_dir_path(__FILE__) . '/widget/recent-post.php';

// Load Redux extensions
define( 'MY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'MY_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
if ( file_exists( plugin_dir_path( __FILE__ ) . '/redux/redux-extensions/loader.php' ) ) {
	require_once plugin_dir_path( __FILE__ ) . '/redux/redux-extensions/loader.php';
}